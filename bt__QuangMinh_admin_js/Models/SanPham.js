function SanPham (
    id,
    name,
    img,
    description,
    price,
    inventory,
    rating,
    type
){
    this.id = id;
    this.name = name;
    this.img = img;
    this.description = description;
    this.price = price;
    this.inventory = inventory;
    this.rating = rating;
    this.type = type;
}