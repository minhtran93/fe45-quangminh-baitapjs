function getproductsList() {
    return axiosProducts ({
      method: "GET",
      url: "products",
    });
  }

  function updateProducts(products) {
    return axiosProducts({
      method: "POST",
      url: "products",
      data: products,
    });
  }
  
  function deleteProducts(id) {
    return axiosProducts({
      method: "DELETE",
      url: `products/${id}`,
    });
  }
  
  function getProducts(id) {
    return axiosProducts({
      method: "GET",
      url: `products/${id}`,
    });
  }
  
  function updatePros(id, products) {
    return axiosProducts({
      method: "PUT",
      url: `products/${id}`,
      data: products,
    });
  }